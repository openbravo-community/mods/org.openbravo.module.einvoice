/************************************************************************************ 
 * Copyright (C) 2009-2022 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/

package org.openbravo.module.einvoice.ad_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.xmlEngine.XmlDocument;

public class Generate extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  private FileGeneration fileGeneration = new FileGeneration();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Posted: doPost");

    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strKey = vars.getGlobalVariable("inpcInvoiceId", "eInvoice|C_Invoice_ID");
      String strWindowId = vars.getGlobalVariable("inpwindowId", "eInvoice|windowId");
      String strTabId = vars.getGlobalVariable("inpTabId", "eInvoice|tabId");
      String strProcessId = vars.getGlobalVariable("inpProcessed", "eInvoice|processId", "");
      String strPath = vars.getGlobalVariable("inpPath", "eInvoice|path",
          strDireccion + request.getServletPath());
      String strPosted = vars.getGlobalVariable("inpposted", "eInvoice|posted");
      String strDocNo = vars.getRequestGlobalVariable("inpdocumentno", "eInvoice|docNo");
      String strWindowPath = Utility.getTabURL(strTabId, "E", true);
      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet;

      if (!strPosted.equals("Y"))
        advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()),
            Utility.messageBD(this, "ERROR", vars.getLanguage()));
      xmlEngine.readXmlTemplate("org/openbravo/module/einvoice/ad_process/Generate")
          .createXmlDocument();
      printPage(response, vars, strKey, strWindowId, strTabId, strProcessId, strPath, strPosted,
          strWindowPath, strDocNo);
    } else if (vars.commandIn("SAVE")) {
      String strKey = vars.getRequestGlobalVariable("inpcInvoiceId", "eInvoice|C_Invoice_ID");
      String strWindowId = vars.getRequestGlobalVariable("inpwindowId", "eInvoice|windowId");
      String strTabId = vars.getRequestGlobalVariable("inpTabId", "eInvoice|tabId");
      String strProcessId = vars.getRequestGlobalVariable("inpProcessed", "eInvoice|processId");
      String strPath = vars.getRequestGlobalVariable("inpPath", "eInvoice|path");
      String strPosted = vars.getRequestGlobalVariable("inpposted", "eInvoice|posted");
      String strDocNo = vars.getRequestGlobalVariable("inpdocumentno", "eInvoice|docNo");
      String strWindowPath = Utility.getTabURL(strTabId, "E", true);
      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet;

      OBError myMessage = process(response, vars, strKey, strWindowId, strTabId, strProcessId,
          strPath, strPosted, strWindowPath, strDocNo);
      vars.setMessage("Generate", myMessage);
      if (myMessage.getType().equals("Error"))
        printPage(response, vars, strKey, strWindowId, strTabId, strProcessId, strPath, strPosted,
            strWindowPath, strDocNo);
    } else {
      advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()),
          Utility.messageBD(this, "ERROR", vars.getLanguage()));
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strFilePath, String strDocNo) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button process Generate Electronic Invoice");

    String[] discard = { "" };
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/module/einvoice/ad_process/Generate", discard).createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);
    xmlDocument.setParameter("processed", strProcessId);
    xmlDocument.setParameter("path", strPath);
    xmlDocument.setParameter("posted", strPosted);
    xmlDocument.setParameter("docNo", strDocNo);

    {
      OBError myMessage = vars.getMessage("Generate");
      vars.removeMessage("Generate");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("cInvoiceId", strKey);

    if (!strFilePath.equals("")) {
      xmlDocument.setParameter("inpFilePath", strFilePath);
      xmlDocument.setParameter("inpOnLoad", "onLoadClose();");
    }
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private OBError process(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strWindowPath, String strDocNo) {
    OBError myMessage = new OBError();
    myMessage.setType("Error");
    myMessage.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

    List<Invoice> li = null;
    try {
      OBContext.setAdminMode(true);
      final OBCriteria<Invoice> inv = OBDal.getInstance().createCriteria(Invoice.class);
      inv.add(Restrictions.eq(Invoice.PROPERTY_ID, strKey));
      li = inv.list();
    } catch (HibernateException e) {
      myMessage.setMessage(Utility.messageBD(this, "Einv_Hibernate_Error\n" + e.getMessage(),
          vars.getLanguage()));
      log4j.error("Error during process", e);
      return myMessage;
    } finally {
      OBContext.restorePreviousMode();
    }
    if (li == null || li.size() == 0) {
      return myMessage;
    }
    final Invoice i = li.get(0);

    i.setProcessNow(true);

    String strResult = fileGeneration.process((ConnectionProvider) this, response, vars,
        globalParameters, i, strKey, strTab, strPosted);

    if (strResult.equals("Success")) {
      try {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/xml");
        response.setHeader("Content-Disposition", "attachment; filename=" + strDocNo + ".xml");
        PrintWriter out = response.getWriter();
        out.println((Utility.fileToString(fileGeneration.file.getAbsolutePath())));
        out.close();
        myMessage.setType("Success");
        myMessage.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
        i.setEinvGenerated("Y");
        i.setProcessNow(false);
      } catch (IOException e) {
        log4j.error("Error during process", e);
        i.setProcessNow(false);
        myMessage.setMessage(Utility.messageBD(this, "FileCannotCreate", vars.getLanguage()));
        return myMessage;
      }
    } else {
      i.setProcessNow(false);
      myMessage.setMessage(strResult);
    }
    return myMessage;
  }
}
