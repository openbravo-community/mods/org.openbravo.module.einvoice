/************************************************************************************ 
 * Copyright (C) 2009-2022 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/

package org.openbravo.module.einvoice.ad_process;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openbravo.base.ConfigParameters;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.module.einvoice.templates.OBEInvoice_I;

public class FileGeneration {

  private static final Logger log = LogManager.getLogger();
  public File file;

  public String process(ConnectionProvider con, HttpServletResponse response,
      VariablesSecureApp vars, ConfigParameters globalParameters, Invoice i, String strKey,
      String strTabId, String strPosted) {
    OBEInvoice_I classEinvoice = null;
    String strJavaClass = null;
    try {
      OBContext.setAdminMode(true);
      if (!i.getBusinessPartner().isEinvEinvoice()) {
        return Utility.messageBD(con, "Einv_BPartner_Not_EInvoice", vars.getLanguage());
      }
      strJavaClass = i.getBusinessPartner().getEinvFormat().getSearchKey();
      classEinvoice = (OBEInvoice_I) Class.forName(strJavaClass).getDeclaredConstructor().newInstance();
    } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
      log.error("Exception during process", e);
      return Utility.messageBD(con, "Error", vars.getLanguage());
    } catch (ClassNotFoundException e) {
      log.error("Exception during process", e);
      return Utility.messageBD(con, "EINV_ClassNotFound", vars.getLanguage()) + " " + strJavaClass;
    } finally {
      OBContext.restorePreviousMode();
    }
    return writeFile(con, vars, globalParameters, classEinvoice, i);
  }

  private String writeFile(ConnectionProvider con, VariablesSecureApp vars,
      ConfigParameters globalParameters, OBEInvoice_I classEinvoice, Invoice invoice) {

    final String strFTPDirectory = globalParameters.strFTPDirectory;
    if (!new File(strFTPDirectory).canWrite()) {
      return Utility.messageBD(con, "CannotWriteDirectory", vars.getLanguage()) + " "
          + strFTPDirectory;
    }

    (new File(strFTPDirectory + "/Facturae31")).mkdir();
    final String rootDirectory = strFTPDirectory + "/Facturae31/";

    try {
      file = classEinvoice.generateFile(invoice, rootDirectory, vars.getLanguage());
    } catch (Exception e) {
      log.error("Exception during writeFile", e);
      return Utility.messageBD(con, e.getMessage(), vars.getLanguage());
    }
    return "Success";
  }
}